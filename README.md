# Ecdosis tutorials for Linux
## Installation
1. Please download the tutorials-linux.tar.gz file to your hard disk. You will find it under the "downloads" item in the Bitbucket menu for this repository (tutorials-linux).
2. Using the terminal program cd to the directory where you downloaded (usually /home/<username>/Downloads). You might like to move it to your home directory with the commmand:
mv tutorials-linux.tar.gz ~; cd
3. Unpack it using the command: tar -xf tutorials-linux.tar.gz
4. cd into the tutorials-linux folder.
5. Run the appication via the command:
./ecdosis-run.sh
6. Go to a web-browser and type in the URL bar: http://localhost:8081/
7. To stop ecdosis go to the tutorials-linux folder again in the terminal and type:
./ecdosis-stop.sh
8. To uninstall tutorials-linux simply delete the folder, after first stopping the application as in step 7.
